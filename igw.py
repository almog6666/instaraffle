import hashlib
import string
import random
from instagram_web_api import Client, ClientCompatPatch, ClientError, ClientLoginError
import datetime
from time import sleep
from collections import Counter


class MyClient(Client):

    @staticmethod
    def _extract_rhx_gis(html):
        options = string.ascii_lowercase + string.digits
        text = ''.join([random.choice(options) for _ in range(8)])
        return hashlib.md5(text.encode()).hexdigest()

    def login(self):
        """Login to the web site."""
        if not self.username or not self.password:
            raise ClientError('username/password is blank')

        time = str(int(datetime.datetime.now().timestamp()))
        enc_password = f"#PWD_INSTAGRAM_BROWSER:0:{time}:{self.password}"

        params = {'username': self.username, 'enc_password': enc_password,
                  'queryParams': '{}', 'optIntoOneTap': False}
        self._init_rollout_hash()
        login_res = self._make_request(
            'https://www.instagram.com/accounts/login/ajax/', params=params)
        if not login_res.get('status', '') == 'ok' or not login_res.get('authenticated'):
            raise ClientLoginError('Unable to login')

        if self.on_login:
            on_login_callback = self.on_login
            on_login_callback(self)
        return login_res


# Without any authentication
web_api = MyClient(auto_patch=False, drop_incompat_keys=False)
short_code = ''


def get_all_comments(short_code, amount=None):
    data = []
    info = web_api.media_comments(short_code, count=50, extract=False)
    comments_count = get_comments_count(info)
    amount = amount or comments_count
    print('Limit = ', amount)
    end_cursor = get_end_cursor(info)
    users = extract_users(info)
    data.extend(users)
    print(f'Collecting users  {len(data)}/{amount}')

    throttled_cursor = None

    while end_cursor and len(data) < amount:
        try:
            if throttled_cursor:
                end_cursor = throttled_cursor
                throttled_cursor = None
            else:
                info = web_api.media_comments(
                    short_code, count=50, end_cursor=end_cursor, extract=False)
                end_cursor = get_end_cursor(info)  # get new cursor
                users = extract_users(info)
                data.extend(users)
                print(f'Collecting users  {len(data)}/{amount}')
        except:
            throttled_cursor = end_cursor
            print('Throttled need to wait..')
            sleep(5)

    return data


def extract_users(info):
    return list(map(lambda x: x['owner']['username'], [c['node'] for c in info.get('data', {}).
                                                       get('shortcode_media', {}).get('edge_media_to_comment', {}).get('edges', [])]))


def get_end_cursor(info):
    return info.get('data', {}).get('shortcode_media', {}).get('edge_media_to_comment', {}).get('page_info', {}).get('end_cursor', None)


def get_comments_count(info):
    return info.get('data', {}).get('shortcode_media', {}).get('edge_media_to_comment', {}).get('count', None)


# web_api.media_info2(short_code)
# print(test(short_code))
usernames = get_all_comments(short_code)
print(random.choices(usernames, k=5))
print(Counter(usernames))
